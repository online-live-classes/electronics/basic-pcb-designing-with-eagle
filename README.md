### Prerequisites

* Basic knowledge of Electronics

### Course Content

#### Day 1

* What is Electronics?
* Why A PCB is required?
* Types of PCB
* Important Features and Specifications of PCBs
* What are the things to know before starting a PCB design?

#### Day 2

* Setup and planning before starting a PCB Design
* Electronics fundamentals in PCB Design

#### Day 3

* Eagle Schematic and Tools
* Testing a Schematic
* Common mistakes and how to avoid them

#### Day 4

* Eagle Board Layout creation
* Layer concepts and stacking

#### Day 5

* Few important tactics
* Making the board production ready